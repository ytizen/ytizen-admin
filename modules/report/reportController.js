
import path from 'path';
import {
  createReport,
  getReports,
  updateReport as updateReportQuery
} from './reportRepository';

const templateDir = 'modules/report/views';

/**
 * GET /reports
 * Reports form page.
 */
const getReportsList = async (req, res) => {
  const reports = await getReports();

  res.render(path.join(templateDir, 'reportsList.twig'), {
    reports
  });
};

/**
 * GET /reports/add
 */
const addReport = (req, res) => {
  res.render(path.join(templateDir, 'reportsAdd.twig'));
};

/**
 * POST /report
 * Create new report
 */
const postReport = async (req, res) => {
  const {
    subject, description, apartmentId, residentId, streetAddress, cityAddress, zipcodeAddress,
    floorAddress, stairAddress, isUrgent, scope, category
  } = req.body;

  const address = {
    street: streetAddress,
    city: cityAddress,
    zipCode: zipcodeAddress,
    additional: {
      floor: floorAddress,
      stair: stairAddress
    }
  };

  const resident = { id: residentId, apartmentId };

  console.log('files', req.files);

  const photos = [];
  Object.values(req.files).forEach((file) => {
    photos.push({ path: `uploads/${file.filename}` });
  });

  console.log('Try to create new report ', subject, description, resident, address, isUrgent, scope, category, photos);

  const newReport = await createReport({
    subject, description, resident, address, photos, isUrgent, scope, category
  });

  console.log('New report', newReport);

  return res.redirect('/reports');
};

/**
 * POST /reports/update/:id
 * Update a report
 */
const updateReport = async (req, res) => {
  const { id } = req.params;

  // const partnerInputName = `partner-${id}`;

  // const partner = req.body[partnerInputName];

  const status = 'Traite';

  console.log('Try to update report #', id, 'for fields', status);

  const updatedReport = await updateReportQuery({ id, status });

  console.log('Report updated', updatedReport);

  return res.redirect('/reports');
};

/**
 * GET /api/reports
 * Return report data.
 */
const getReportsData = async (req, res) => {
  let filters;

  if (req.query) {
    if (req.query.scope) {
      filters = {
        scope: req.query.scope
      };
    }

    if (req.query.date && req.query.date !== 'undefined') {
      filters = {
        ...filters,
        date: req.query.date
      };
    }
  }

  console.log('Trying to get reports for filters ', filters);

  const reports = await getReports(filters);

  return res.json({ data: reports });
};

export {
  getReportsList,
  postReport,
  addReport,
  updateReport,
  getReportsData
};
