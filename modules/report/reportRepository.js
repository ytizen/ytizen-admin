import { Types } from 'mongoose';
import Report from './report';

const createReport = async (inputData) => {
  const report = new Report({
    subject: inputData.subject,
    description: inputData.description,
    resident: inputData.resident,
    address: inputData.address,
    photos: inputData.photos,
    isUrgent: inputData.isUrgent,
    status: inputData.status,
    scope: inputData.scope,
    category: inputData.category
  });

  try {
    return await report.save();
  } catch (err) {
    return err;
  }
};

const getConditionsByFilters = (filters) => {
  let conditions = {};
  if (filters.category) {
    conditions = {
      ...conditions,
      category: filters.category
    };
  }

  if (filters.scope) {
    conditions = {
      ...conditions,
      scope: filters.scope
    };
  }

  if (filters.date) {
    const ltDate = new Date(filters.date);

    ltDate.setDate(ltDate.getDate() + 1);

    conditions = {
      ...conditions,
      createdAt: {
        $gte: filters.date,
        $lt: ltDate
      }
    };
  }

  return conditions;
};

const findReportById = async (id) => {
  try {
    return await Report.findById(id);
  } catch (err) {
    return err;
  }
};

const updateReport = async (inputData) => {
  let update = {};
  if (inputData.status) {
    update = {
      ...update,
      status: inputData.status
    };
  }

  if (inputData.partner) {
    update = {
      ...update,
      partner: inputData.partner
    };
  }

  try {
    const report = await Report.findOneAndUpdate({ _id: inputData.id }, update, { new: true });
    if (!report) {
      throw new Error(`Impossible de trouver le signalement avec id: ${inputData.id}`);
    }

    return report;
  } catch (err) {
    return err;
  }
};

const getReports = async (filters, limit, sortField, sortOrder) => {
  let conditions = {};
  if (filters) {
    conditions = getConditionsByFilters(filters);
  }

  try {
    return await Report.find(conditions)
      .sort({ [sortField]: sortOrder })
      .limit(limit);
  } catch (err) {
    return err;
  }
};

const groupReportsByType = async (type) => {
  try {
    return await Report.aggregate([
      { $group: { _id: `$${type}`, reports: { $push: '$$ROOT' }, count: { $sum: 1 } } }
    ]);
  } catch (err) {
    return err;
  }
};

const findReportsByIds = async (reportIds) => {
  try {
    const ids = reportIds.map(id => Types.ObjectId(id));
    return await Report.find({ _id: { $in: ids } });
  } catch (err) {
    return err;
  }
};

export {
  createReport,
  updateReport,
  getReports,
  findReportById,
  findReportsByIds,
  groupReportsByType
};
