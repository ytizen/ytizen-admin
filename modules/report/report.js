import { Schema, model } from 'mongoose';

const { ObjectId } = Schema;

const scopes = Object.freeze({
  APPARTEMENT: 'Appartement',
  IMMEUBLE: 'Immeuble',
  COMMUNE: 'Commune'
});

const status = Object.freeze({
  NON_TRAITE: 'Non traite',
  EN_COURS_DE_TRAITEMENT: 'En cours de traitement',
  EN_ATTENTE: 'En attente',
  TRAITE: 'Traite'
});

const categories = Object.freeze({
  ELECTRICITE: 'Electricité',
  ECLAIRAGE: 'Eclairage',
  ASCENSEUR: 'Ascenseur',
  PLOMBERIE: 'Plomberie',
  JARDIN: 'Jardin',
  SECURITE: 'Sécurité',
  ETANCHEITE: 'Etancheité',
  VITRAGE: 'Vitrage',
  HUMIDITE: 'Humidite',
  AIR_DE_JEU: 'Air de jeu',
  INSALUBRITE: 'Insalubrité',
  AUTRES: 'Autres'
});

const reportSchema = new Schema({
  subject: {
    type: String,
    required: [true, 'Sujet est requis.']
  },
  description: {
    type: String
  },
  resident: {
    id: { type: String, required: true },
    apartmentId: { type: String, required: true },
  },
  address: {
    street: { type: String },
    city: { type: String },
    zipCode: { type: String },
    additional: {
      floor: Number,
      stair: String
    }
  },
  scope: {
    type: String,
    enum: Object.values(scopes),
    required: [true, 'Scope est requis.']
  },
  status: {
    type: String,
    enum: Object.values(status),
    default: status.NON_TRAITE,
    required: [true, 'Status est requis.']
  },
  category: {
    type: String,
    enum: Object.values(categories),
    required: [true, 'Catégorie est requis.']
  },
  isUrgent: {
    type: Boolean,
    default: false,
    required: [true, 'isUrgent est requise']
  },
  photos: [
    {
      path: { type: String },
    }
  ],
  partner: {
    id: { type: ObjectId, ref: 'Partner' },
    intervenedAt: { type: Date }
  }
}, { timestamps: true });

export default model('Report', reportSchema);
