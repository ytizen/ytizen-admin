import { Schema, model } from 'mongoose';

const roles = Object.freeze({
  GARDIEN: 'Gardien',
  CHARGE_DE_PROXIMITE: 'Chargé de proximité',
  MANAGER: 'Manager'
});

const zones = Object.freeze({
  ALL: 'Toutes les zones',
  ZONE_A: 'Zone A',
  ZONE_B: 'Zone B',
  ZONE_C: 'Zone C'
});

const buildings = Object.freeze({
  ALL: 'Tous le Parc immobilier',
  RESIDENCE_RIVIERA: 'Residence Riviera',
  RESIDENCE_TAHITY: 'Residence Tahity',
  RESIDENCE_EVO: 'Residence Evo',
  RESIDENCE_CAROUGE: 'Residence Carouge'
});

const collaboratorSchema = new Schema({
  lastName: { type: String, required: true },
  firstName: { type: String },
  phone: { type: String },
  email: { type: String, unique: true },
  role: {
    type: String,
    enum: Object.values(roles),
    required: true
  },
  zones: [{
    type: String,
    enum: Object.values(zones),
  }],
  buildings: [{
    type: String,
    enum: Object.values(buildings),
  }]
}, { timestamps: true });

export default model('Collaborator', collaboratorSchema);
