import Collaborator from './collaborator';

const createCollaborator = async (inputData) => {
  const collaborator = new Collaborator({
    firstName: inputData.firstName,
    lastName: inputData.lastName,
    phone: inputData.phone,
    email: inputData.email,
    role: inputData.role,
    zones: inputData.zones,
    buildings: inputData.buildings
  });

  try {
    return await collaborator.save();
  } catch (err) {
    return err;
  }
};

const updateCollaborator = async (inputData) => {
  let update = {};
  if (inputData.lastName) {
    update = {
      ...update,
      lastName: inputData.lastName
    };
  }

  if (inputData.firstName) {
    update = {
      ...update,
      firstName: inputData.firstName
    };
  }

  if (inputData.email) {
    update = {
      ...update,
      email: inputData.email
    };
  }

  if (inputData.phone) {
    update = {
      ...update,
      phone: inputData.phone
    };
  }

  if (inputData.role) {
    update = {
      ...update,
      role: inputData.role
    };
  }

  if (inputData.zones) {
    update = {
      ...update,
      zones: inputData.zones
    };
  }

  if (inputData.buildings) {
    update = {
      ...update,
      buildings: inputData.buildings
    };
  }

  try {
    const collaborator = await Collaborator
      .findOneAndUpdate({ _id: inputData.id }, update, { new: true });
    if (!collaborator) {
      throw new Error(`Impossible de trouver le collaborateur avec id: ${inputData.id}`);
    }

    return collaborator;
  } catch (err) {
    return err;
  }
};

const getCollaborators = async (limit, sortField, sortOrder) => {
  try {
    return await Collaborator.find()
      .sort({ [sortField]: sortOrder })
      .limit(limit);
  } catch (err) {
    return err;
  }
};

const findCollaboratorByConditions = async (conditions) => {
  try {
    return await Collaborator
      .findOne(conditions);
  } catch (err) {
    return err;
  }
};

export {
  createCollaborator,
  getCollaborators,
  updateCollaborator,
  findCollaboratorByConditions
};
