import path from 'path';
import {
  createCollaborator,
  getCollaborators,
  updateCollaborator as updateCollaboratorQuery
} from './collaboratorRepository';

const templateDir = 'modules/collaborator/views';

/**
 * GET /collaborators
 * Collaborators form page.
 */
const getCollaboratorsList = async (req, res) => {
  const collaborators = await getCollaborators();
  res.render(path.join(templateDir, 'collaboratorsList.twig'), {
    collaborators
  });
};

/**
 * GET /collaborators/add
 */
const addCollaborator = (req, res) => {
  res.render(path.join(templateDir, 'collaboratorsAdd.twig'));
};

/**
 * POST /collaborator
 * Create new collaborator
 */
const postCollaborator = async (req, res) => {
  const {
    lastName, firstName, phone, email, buildings, role
  } = req.body;

  console.log('Try to create new collaborator ', lastName, firstName, phone, email, buildings, role);

  const newCollaborator = await createCollaborator({
    lastName, firstName, phone, email, buildings, role
  });

  console.log('New collaborator', newCollaborator);

  return res.redirect('/collaborators');
};

/**
 * POST /collaborators/update/:id
 * Update a collaborator
 */
const updateCollaborator = async (req, res) => {
  const { id } = req.params;

  const buildingsInputName = `buildings-${id}`;
  const zonesInputName = `zones-${id}`;
  const roleInputName = `role-${id}`;

  const buildings = req.body[buildingsInputName];
  const zones = req.body[zonesInputName];
  const role = req.body[roleInputName];

  console.log('Try to update collaborator #', id, 'for fields', buildings, zones, role);

  const updatedCollaborator = await updateCollaboratorQuery({
    id, buildings, zones, role
  });

  console.log('Collaborator updated', updatedCollaborator);

  return res.redirect('/collaborators');
};

export {
  getCollaboratorsList,
  postCollaborator,
  addCollaborator,
  updateCollaborator
};
