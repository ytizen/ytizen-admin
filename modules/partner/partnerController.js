
import path from 'path';
import {
  createPartner,
  getPartners,
  updatePartner as updatePartnerQuery
} from './partnerRepository';

const templateDir = 'modules/partner/views';

/**
 * GET /partners
 * Partners form page.
 */
const getPartnersList = async (req, res) => {
  const partners = await getPartners();
  console.log(partners);
  res.render(path.join(templateDir, 'partnersList.twig'), {
    partners
  });
};

/**
 * GET /partners/add
 */
const addPartner = (req, res) => {
  res.render(path.join(templateDir, 'partnersAdd.twig'));
};

/**
 * POST /partner
 * Create new partner
 */
const postPartner = async (req, res) => {
  const {
    companyName, contactName, phone, zones, category
  } = req.body;

  console.log('Try to create new partner ', companyName, contactName, phone, zones, category);

  const newPartner = await createPartner({
    companyName, contactName, phone, zones, category
  });

  console.log('New partner', newPartner);

  return res.redirect('/partners');
};

/**
 * POST /partners/update/:id
 * Update a partner
 */
const updatePartner = async (req, res) => {
  const { id } = req.params;

  const zonesInputName = `zones-${id}`;

  const zones = req.body[zonesInputName];

  console.log('Try to update partner #', id, 'for fields', zones);

  const updatedPartner = await updatePartnerQuery({ id, zones });

  console.log('Partner updated', updatedPartner);

  return res.redirect('/partners');
};

export {
  getPartnersList,
  postPartner,
  addPartner,
  updatePartner
};
