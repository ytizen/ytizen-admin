import { Schema, model } from 'mongoose';

const zones = Object.freeze({
  ZONE_A: 'Zone A',
  ZONE_B: 'Zone B',
  ZONE_C: 'Zone C',
  ZONE_D: 'Zone D'
});

const categories = Object.freeze({
  PLOMBIER: 'Plombier',
  CHAUFFAGISTE: 'Chauffagiste',
  TECHNICIEN_MAINTENANCE: 'Technicien maintenance',
  MENUISIER: 'Menuisier',
  EBOUEUR: 'Eboueur',
  TECHNICIEN: 'Technicien',
  MENAGE: 'Menage'
});

const partnerSchema = new Schema({
  companyName: { type: String, required: true },
  contactName: { type: String, required: true },
  phone: { type: String, required: true },
  zones: [{
    type: String,
    enum: Object.values(zones),
    required: true
  }],
  category: {
    type: String,
    enum: Object.values(categories),
    required: true
  },
  rating: {
    type: Number,
    enum: [1, 2, 3, 4, 5],
  },
}, { timestamps: true });

export default model('Partner', partnerSchema);
