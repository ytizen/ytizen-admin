import Partner from './partner';

const createPartner = async (inputData) => {
  const partner = new Partner({
    companyName: inputData.companyName,
    contactName: inputData.contactName,
    phone: inputData.phone,
    zones: inputData.zones,
    category: inputData.category,
    rating: Math.floor(Math.random() * 4) + 1
  });

  try {
    return await partner.save();
  } catch (err) {
    return err;
  }
};

const updatePartner = async (inputData) => {
  let update = {};

  if (inputData.companyName) {
    update = {
      ...update,
      companyName: inputData.companyName
    };
  }

  if (inputData.contactName) {
    update = {
      ...update,
      contactName: inputData.contactName
    };
  }

  if (inputData.phone) {
    update = {
      ...update,
      phone: inputData.phone
    };
  }

  if (inputData.category) {
    update = {
      ...update,
      category: inputData.category
    };
  }

  if (inputData.zones) {
    update = {
      ...update,
      zones: inputData.zones
    };
  }

  try {
    const partner = await Partner
      .findOneAndUpdate({ _id: inputData.id }, update, { new: true });
    if (!partner) {
      throw new Error(`Impossible de trouver le partenaire avec id: ${inputData.id}`);
    }

    return partner;
  } catch (err) {
    return err;
  }
};

const getPartners = async (limit, sortField, sortOrder) => {
  try {
    return await Partner.find()
      .sort({ [sortField]: sortOrder })
      .limit(limit);
  } catch (err) {
    return err;
  }
};

const findPartnerByConditions = async (conditions) => {
  try {
    return await Partner
      .findOne(conditions);
  } catch (err) {
    return err;
  }
};

export {
  createPartner,
  getPartners,
  updatePartner,
  findPartnerByConditions
};
